angular.module('manageDataApp', [])
    .controller('ManageDataController', function() {
        const manageData = this;
        manageData.colorList = [];
        manageData.sizeList = [];
        manageData.materialList = [];
        manageData.color = '';
        manageData.size = '';
        manageData.material = '';
        manageData.colorGroupList = [];
        manageData.generatedJSON = '';

        manageData.updateSizeStock = function(data) {
            let sizeStock = 0;
            for (const sizeGroup of data.variation[0].variation) {
                sizeStock += parseInt(sizeGroup.stock);
            }
            return sizeStock;
        };

        manageData.updateColorStock = function(data) {
            let colorStock = 0;
            for (const colorGroup of data.variation[0].variation) {
                colorStock += parseInt(colorGroup.stock);
            }
            return colorStock;
        };

        manageData.updateTotalStock = function(data) {
            let totalStock = 0;
            for (const totalGroup of data.variation) {
                totalStock += parseInt(totalGroup.stock);
            }
            return totalStock;
        };

        manageData.getInputData = function() {
            const getMaterialDataList = (sizeIndex) => {
                const materialDataList = [];
                manageData.materialList.forEach((material, i) => {
                    const obj = {
                        "attribute": false,
                        "name": material,
                        "cost": "",
                        "totalcost": "",
                        "stock": "0"
                    };
                    materialDataList.push(obj);
                });
                return materialDataList;
            };

            const getSizeDataList = (colorIndex) => {
                const sizeDataList = [];
                manageData.sizeList.forEach((size, i) => {
                    const obj = {
                        "attribute": false,
                        "name": size,
                        "stock": 0,
                        "variation": [
                            {
                                "attribute": true,
                                "name": "Material",
                                "variation": getMaterialDataList(i)
                            }
                        ]
                    };
                    sizeDataList.push(obj);
                });
                return sizeDataList;
            };

            const getColorDataList = () => {
                const colorDataList = [];
                manageData.colorList.forEach((color, i) => {
                    const obj = {
                        "attribute": false,
                        "name": color,
                        "stock": 0,
                        "variation": [
                            {
                                "attribute": true,
                                "name": "Size",
                                "variation": getSizeDataList(i)
                            }
                        ]
                    };
                    colorDataList.push(obj);
                });
                return colorDataList;
            };

            manageData.colorGroupList = {
                "stock": 0,
                "variation": getColorDataList(),
                "attribute": true,
                "name": "Color"
            };
        };

        manageData.generateTable = function() {
            if (manageData.color && manageData.size && manageData.material) {
                manageData.colorList = manageData.color.replace(/\s/g, '').split(',');
                manageData.sizeList = manageData.size.replace(/\s/g, '').split(',');
                manageData.materialList = manageData.material.replace(/\s/g, '').split(',');
                manageData.getInputData();
            } else {
                alert('Input Attribute values !')
            }
        };

        manageData.generateJSON = function() {
            let json = angular.toJson( manageData.colorGroupList );
            json = JSON.parse(json);
            json = JSON.stringify(json, null, 2);
            manageData.generatedJSON = json;

            setTimeout(function () {
                window.scrollTo(0,document.body.scrollHeight);
            }, 0);

            console.log('dataParam: ', manageData.generatedJSON);
        };
    });